﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Xml.Linq;
using TarAdmin.Helper;


namespace TarAdmin.Areas.Replication.Controllers
{
    public class ReplicationController : Controller
    {
        #region Global Properties
        string sXMLFile = ConfigurationManager.AppSettings["RTest"].ToString();
        string _logFile = ConfigurationManager.AppSettings["LogFile"].ToString();
        #endregion

        // GET: Replication/Replication
        public ActionResult Index()
        {
            return View();
        }

        #region ReplicateFiles
        [HttpPost]
        public JsonResult ReplicateFiles()
        {
            Replicator replicatorObj = new Replicator(HttpContext.Server.MapPath("~/App_Data/" + sXMLFile), _logFile);
            replicatorObj.Replicate();
            return Json("done");
        }
        #endregion

        // GET: Replication/Replication/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Replication/Replication/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Replication/Replication/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Replication/Replication/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Replication/Replication/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Replication/Replication/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Replication/Replication/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        //public void Replicate()
        //{
        //    //ReadXML(@"C:\\Users\\Hp\\Documents\\Visual Studio 2013\\Projects\\TarAdmin\\TarAdmin\\App_Data\\XML\\RTest.xml");
        //    //ReadXML(HttpContext.Server.MapPath("~/App_Data/" + ConfigurationManager.AppSettings["RTest"].ToString()));
        //}

        //#region ReadXML
        //public void ReadXML(string xmlFile)
        //{
        //    XDocument document = XDocument.Load(xmlFile);

        //    var jobs = document.Descendants("job");

        //}
        //#endregion
    }
}
