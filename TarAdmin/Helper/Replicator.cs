﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using TarAdmin.Models.Replication;
using System.IO;
using System.Collections;

namespace TarAdmin.Helper
{
    public class Replicator
    {
        #region Global Properties
        #endregion

        #region Properties
        private string XMLFile { get; set; }
        private string LogFile { get; set; }
        private int fCount { get; set; }
        #endregion

        #region Constructor
        public Replicator(string xmlFile, string logFile)
        {
            XMLFile = xmlFile;
            LogFile = logFile;
        }
        #endregion

        #region Replicate
        public void Replicate()
        {
            StartReplication(XMLFile);
        }
        #endregion

        #region StartReplication
        private int StartReplication(string xmlFile)
        {
            List<ReplicationModel> replicateList = new List<ReplicationModel>();

            fCount = 0;
            var result = 0;

            using (var stream = new StreamReader(xmlFile))
            {
                var xDocs = XDocument.Load(stream);
                var query = from j in xDocs.Descendants("jobs") select j;
                foreach (var job in query.AsEnumerable())
                {
                    replicateList.Add(new ReplicationModel
                    {
                        JobId = Convert.ToInt32(job.Element("job").Attribute("id").Value),
                        JobDescription = job.Element("job").Attribute("description").Value,
                        ItemId = Convert.ToInt32(job.Element("item").Attribute("id").Value),
                        ItemDescription = job.Element("item").Attribute("description").Value,
                        Type = job.Element("type").Value,
                        Filter = Convert.ToInt32(job.Element("filter").Value),
                        Days = Convert.ToInt32(job.Element("days").Value),
                        DisplayWarning = Convert.ToInt32(job.Element("displaywarning").Value),
                        DelSource = Convert.ToInt32(job.Element("delsource").Value),
                        Source = job.Element("source").Value,
                        Destination = job.Element("destination").Value
                    });
                }
            }


            foreach (var job in replicateList)
            {
                try
                {

                    if (job.Type.Trim().ToLower() == "file")
                    {
                        result = CopyFile(job.Source, job.Destination, string.Empty.PadRight(11) + job.ItemId + " " + job.ItemDescription.PadRight(33, ' '), job.DisplayWarning, job.Filter, job.Days, job.DelSource);
                    }
                    else if (job.Type.Trim().ToLower() == "folder")
                    {
                        result = CopyFolder(job.Source, job.Destination, string.Empty.PadRight(11) + job.ItemId + " " + job.ItemDescription.PadRight(33, ' '), job.DisplayWarning, job.Filter, job.Days, job.DelSource);
                    }

                    if (result == 1)
                    {
                        FileInfo fn = new FileInfo(job.Source);
                        string sName = null;
                        if (string.IsNullOrEmpty(fn.Name))
                        {
                            sName = job.Source;
                        }
                        else
                        {
                            sName = fn.Name;
                        }
                        WriteLog(string.Empty.PadRight(11) + job.ItemId + " " + job.ItemDescription.PadRight(33, ' ') + " " + sName.PadRight(33, ' ') + " " + "OK");
                    }

                }
                catch (Exception ex)
                {

                    WriteLog("Error ReadXML(): " + ex.Message);
                }
                finally
                {
                    WriteLog("******* End Replication   " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + " *********");
                }
            }

            return fCount;
        }
        #endregion

        #region CopyFile
        private int CopyFile(string sSrcFile, string sDestFile, string sSrcMsg, int iDispWrn, int iFilter = 0, int iDays = 0, int iDelSrc = 0)
        {
            int res = 0;
            FileInfo fSrc = new FileInfo(sSrcFile);
            FileInfo fDst = new FileInfo(sDestFile);

            System.DateTime dGSrc = System.IO.File.GetLastWriteTime(sSrcFile);
            System.DateTime dGTdy = System.IO.File.GetLastWriteTime(sDestFile);

            if (iFilter == 1)
            {
                if (DateTime.Compare(dGSrc, dGTdy) < 0)
                {
                    return 1;
                }
            }

            try
            {
                if (!(fDst.Directory.Exists))
                {
                    fDst.Directory.Create();
                }
                fSrc.CopyTo(sDestFile, true);
                if (fDst.Exists & fDst.Length == fSrc.Length & System.IO.File.GetLastWriteTime(sSrcFile) == System.IO.File.GetLastWriteTime(sDestFile))
                {
                    string sMessage = null;
                    sMessage = fSrc.FullName;
                    res = 1;
                    fCount = fCount + 1;
                }
                else
                {
                    WriteLog(sSrcMsg + " " + fSrc.Name.PadRight(33, ' ') + " " + "Unable to verify file");
                    res = 0;
                }
                System.DateTime dSrc = System.IO.File.GetLastWriteTime(sSrcFile);
                System.DateTime dTdy = DateTime.Now.AddDays(-2);
                if (DateTime.Compare(dSrc, dTdy) < 0 & iDispWrn == 1)
                {
                    WriteLog(sSrcMsg + " " + fSrc.Name.PadRight(33, ' ') + " " + "Warning Old File");
                }
                if (iDelSrc == 1)
                {
                    try
                    {
                        System.IO.File.Delete(fSrc.FullName);
                    }
                    catch
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLog(sSrcMsg + " " + fSrc.Name.PadRight(33, ' ') + " " + ex.Message);
                return res;
            }
            return res;
        }
        #endregion

        #region CopyFolder
        private int CopyFolder(string sSrcPath, string sDestPath, string sSrcMsg, int iDispWrn, int iFilter = 0, int iDays = 0, int iDelSrc = 0)
        {
            int gres = 1;
            int iCnt = 0;
            try
            {
                System.IO.DirectoryInfo sourceDir = new System.IO.DirectoryInfo(sSrcPath);
                System.IO.DirectoryInfo destDir = new System.IO.DirectoryInfo(sDestPath);
                System.IO.FileInfo[] fi = sourceDir.GetFiles();

                foreach (FileInfo fiSrc in fi)
                {
                    int res = 0;
                    res = CopyFile(fiSrc.FullName, System.IO.Path.Combine(sDestPath, fiSrc.Name), sSrcMsg, iDispWrn, iFilter, iDays, iDelSrc);
                    if (res == 0)
                    {
                        gres = 0;
                        iCnt = iCnt + 1;
                    }
                    if (iCnt >= 3)
                    {
                        return 0;
                    }
                }

                foreach (System.IO.DirectoryInfo dir in sourceDir.GetDirectories())
                {
                    CopyFolder(dir.FullName, System.IO.Path.Combine(destDir.FullName, dir.Name), sSrcMsg, iDispWrn, iFilter, iDays, iDelSrc);
                }
            }
            catch (Exception ex)
            {
                WriteLog(sSrcMsg + " " + sSrcPath.PadRight(33, ' ') + " " + ex.Message);
                return 0;
            }
            return gres;
        }
        #endregion

        #region WriteLog
        private void WriteLog(string sMessage)
        {
            System.IO.StreamWriter sw = null;
            try
            {

                if (System.IO.File.Exists(LogFile) == false)
                {
                    sw = System.IO.File.CreateText(LogFile);
                    sw.WriteLine("BackUpApp Log");
                    sw.Flush();
                    sw.Close();
                }
                sw = System.IO.File.AppendText(LogFile);
                sw.WriteLine(sMessage);
                sw.Flush();
                sw.Close();
            }
            catch
            {
                sw.Close();
            }
        }
        #endregion

        #region GetDateNum
        private string GeetDateNum(DateTime d1)
        {
            int rtn = 0;
            rtn = d1.Year * 100 + d1.Month * 1;
            return rtn.ToString();
        }
        #endregion
    }
}
