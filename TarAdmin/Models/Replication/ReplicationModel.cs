﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TarAdmin.Models.Replication
{
    public class ReplicationModel
    {
        public int JobId { get; set; }
        public string JobDescription { get; set; }
        public int ItemId { get; set; }
        public string ItemDescription { get; set; }
        public string Type { get; set; }
        public int Filter { get; set; }
        public int Days { get; set; }
        public int DisplayWarning { get; set; }
        public int DelSource { get; set; }
        public string Source { get; set; }
        public string Destination { get; set; }
    }
}