﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TarAdmin.Startup))]
namespace TarAdmin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
