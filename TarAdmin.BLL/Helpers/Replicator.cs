﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace TarAdmin.BLL
{
    public class Replicator
    {
        #region Private Properties
        //static string _sXMLFilePath = string.Empty;
        //static string _sLogFilePath = string.Empty;
        //static int _iFcnt = 0;

        //<add key="FilePath" value="d:\replication\" />
        //<add key="LogFile" value="d:\Log\logFile.Txt" />
        #endregion

        #region Constructor
        public Replicator() 
        {
        //    _sXMLFilePath = ConfigurationManager.AppSettings["FilePath"].ToString();
        //    _sLogFilePath = ConfigurationManager.AppSettings["LogFile"].ToString();
        }
        #endregion

        #region Replication
        public void Replicate()
        {
            ReadXML(@"d:\replication\");
        }
        #endregion

        #region ReadXML
        public void ReadXML(string xmlFile)
        {
            XDocument document = new XDocument(xmlFile);

            var jobs = document.Descendants("job");

        }
        #endregion
    }
}
